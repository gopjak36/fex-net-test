import random

def random_arr(SIZE, MAX_NUMBER=1000000):
    """ FUnction for get random arr
    Input:  SIZE - size of array
            MAX_NUMBER - max number of arr (optional)
    Return: rand_arr - random array            
    """
    rand_arr = random.sample(range(MAX_NUMBER), k=SIZE)
    return rand_arr

def reverse_array(arr):
    """Function for reverse array 
    Input:  arr - some array
    Return: rev_arr - reversed arr
    """
    rev_array = arr[::-1]
    return rev_array

if __name__ == '__main__':
    arr = random_arr(10000)
    rev_arr = reverse_array(arr)    