import random

from string import ascii_letters

from datetime import datetime

from collections import Counter


ALL_LETTER = ascii_letters

def random_str(SIZE):
    """ Function for create random letter string
    Input:  SIZE - size of random string
    Return: rand_str - random string
    """
    rand_str = ''.join(random.choices(ALL_LETTER, k=SIZE))
    return rand_str

# v0.0.1 #
def search_right_place(list2d, c_count, index=-1, count=0):
    """ Function for search right place for char in 2d list by count 
    Input:  list2d - 2d list
            c_count - char count
            index - current index in list
            count - count of iteration
    Return: r_index - right index to insert
    """
    # current char count by index in 2d list < char count to search:
    if list2d[index][1] <= c_count:
        count += 1
        # call function again (reverse):
        search_right_place(list2d, c_count, index - 1)    
    
    # valid for one loop:    
    if index == -1  and count == 0:
        r_index = 0
    else:        
        r_index = index

    return r_index    

def sort1_str(s_string):
    """ Function for sort char in string with base algo
        from max count of char to min
    Input:  string - some string
    Return: sorted_str - sorted string by char counter (>1)   
    """
    # string to lower case:
    string = s_string.lower()

    # help variables:
    MAX_CHAR_COUNT = 0  
    IGNORE_CHAR = []
    CHAR_2DLIST = []   # Example [['a', 1], ...], 
                       #        where second list is [char, count_of_char]  

    for char in string:
        if char not in IGNORE_CHAR:
            # get count of current char:
            char_count = string.count(char)
            # append current char to ignore char list:
            IGNORE_CHAR.append(char) 
            
            # check if count of current char > 1:
            if char_count > 1:

                # check if count of current char is new max:
                if char_count > MAX_CHAR_COUNT:
                    # new max count:
                    MAX_CHAR_COUNT = char_count
                    # inser char and count of char to first place in list:
                    CHAR_2DLIST.insert(0, [char, char_count])

                elif char_count == MAX_CHAR_COUNT:
                    # index of elemnt with this char count:
                    index = len([l for l in CHAR_2DLIST if l[1] == char_count])
                    # insert new char list to newx index:
                    CHAR_2DLIST.insert(index+1, [char, char_count])

                else:
                    # search right place for current char in list:    
                    r_index = search_right_place(CHAR_2DLIST, char_count)
                    # check if char is count of char min of all char:
                    if r_index == 0:
                        CHAR_2DLIST.append([char, char_count])
                    else:
                        CHAR_2DLIST.insert(r_index, [char, char_count])  

    sorted_str = ''.join([arr[0] for arr in CHAR_2DLIST])

    return sorted_str                   

# v0.0.2 #
def sort2_str(s_string):
    """ Function for sort char in string with listcomprehation and Counter
    Input:  string - some string
    Return: sorted_str - sorted string by char counter (>1)
    """
    # string to lower case:
    string = s_string.lower()

    # get count of char in string as dict:
    d_counter = Counter(string)

    # create string sorted by char counter (>1)
    sorted_str = ''.join([char for char in d_counter if d_counter[char] > 1])

    return sorted_str

if __name__ == '__main__':
    some_string = random_str(400)
    print(f"Some string: {some_string}")
    # v0.0.1
    start1 = datetime.now()
    result1 = sort1_str(some_string)
    end1 = datetime.now() - start1
    print(f"Result of Sorted 1: {result1}\nSorted 1 finised in: {end1}")
    # v0.0.2 (faster, when str count > ~ 400 
                                # but can't sorted corectly the same char count)
    start2 = datetime.now()
    result2 = sort2_str(some_string)
    end2 = datetime.now() - start2
    print(f"Result of Sorted 2: {result2}\nSorted 2 finised in: {end2}")    