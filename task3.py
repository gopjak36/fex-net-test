class Update(object):
    """ Update Dict in Array """

    def __init__(self, dict_arr):
        self.dict_arr = dict_arr

    @property
    def get_dict_arr(self):
        return self.dict_arr   

    def search_key(self, d_id):
        """ Method for search index of dict in dict array by id
        Input:  d_id - value of dict id key
        Return: i - index of dict in dict array
        """
        for i, d in enumerate(self.dict_arr):
            if d.get('id') == d_id:
                return i
        return None        

    def update(self, d_id, new_value):
        index = self.search_key(d_id)
        if index:
            self.dict_arr[index]['state'] = new_value
        return self.dict_arr    
        
                
if __name__ == '__main__':
    array = [
        {'id': 1, 'state': True},
        {'id': 2, 'state': True},
        {'id': 8, 'state': False}
    ]
    print(f'Start array: {array}')
    u = Update(array)
    u.update(2, False)
    print(u.get_dict_arr)
    u.update(8, True)
    print(u.get_dict_arr)
    u.update(9, False)
    print(u.get_dict_arr)