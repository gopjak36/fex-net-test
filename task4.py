import re

NORMALIZE_PATTERN = r'[`\-=~!@#$%^&*()_+\[\]{};\'\\:"|<,./?]|\n|\t|\r|\x0b|\x0c| '

def normalize(string):
    """ Function for normalize string
    Input:  string - some string
    Return: n_string - normalized string
    """
    # split string:
    split_str = re.split(NORMALIZE_PATTERN, string)

    # make string again:
    str_again = ''.join(split_str)

    # delete lst '>':
    if str_again[-1] == '>':
        str_again = str_again[:-1]

    # normalize string:
    n_string = str_again.replace('>', ' > ')

    return n_string

if __name__ == '__main__':
    string_to_normolized = [
        "X > %Y",
        "  X >      Y    >",
        "\"X\" >'Y'> I  \t> 1Z2"
    ]

    for string in string_to_normolized:
        print(f"'{string}' -- normolized --> '{normalize(string)}'")